# Console application #
# for Bash and CMD #
Moves files from one directory to another and renames it in order 01, 02, .., 09, 10, ...  
Creates a new output file and writes the number of files which were moved.  
Subdirectories are ignored.

# Script running syntax #
on Windows:  
moveFiles.bat dir1 dir2 file  

on Linux:  
moveFiles.sh dir1 dir2 file

where:  
dir1 - absolute path to the directory to move files from  
dir2 - absolute path to the directory to move files to  
file - name of the output file. File is created in dir2  
