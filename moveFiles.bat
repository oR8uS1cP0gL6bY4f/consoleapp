@echo off
rem count cmd args
set "args="
for %%x in (%*) do set /A args+=1
rem check and exit if wrong number of arguments
if not %args% == 3 (
	echo Wrong number of arguments. Please provide 3 arguments
	pause
	exit /b 1
)
rem check and exit if directory with files does not exist
IF NOT EXIST "%1" (
	echo Directory to remove files from does not exist
	pause
	exit /b 1
) ELSE (
	echo Directory 1 found
)
rem check and create directory for move files to if dir does not exist
rem exit if unable to create such directory
IF NOT EXIST "%2" (
	echo Directory 2 not found
	echo Trying to create it...
	mkdir "%2"
	IF NOT EXIST "%2" (
		echo Unable to create directory 2
		pause
		exit /b 1
	) ELSE (
		echo Directory "%2" was created
	)
) ELSE (
	echo Directory 2 found
)

rem copy and rename each file from dir1 to dir2
rem inner directories are ignored due to /a-d flag
rem prefix is used for naming first 10 files like 01, 02, ..., 09
SETLOCAL EnableDelayedExpansion
set "count="
set /A count=0
set "prefix=0"
for /f "usebackq delims=|" %%f in (`dir /b /a-d %1`) do (
	echo %%f
	xcopy %1\"%%f" "%2" /v
	Rem Check result code and if it was successful (0), delete the source.
	if errorlevel 0 (
		echo Copy completed successfully
		del /Q "%1"\"%%f"
		echo File deleted
	)
	set /A count+=1
	IF !count! lss 10 (
		ren "%2"\"%%f" %prefix%!count!
	) ELSE (
		ren "%2"\"%%f" !count!
	)
)

rem create (overwrite) output file and write the number of changed files
echo Number of files removed and renamed: %count%
echo Number of files removed and renamed: %count% > "%2"\"%3"
rem reset count variable
set "count="
pause
