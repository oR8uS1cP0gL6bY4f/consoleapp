#!/bin/sh
# dir1 - directory to move files from
# dir2 - directory to move files to

# check the right number of arguments given
# exit if not
args=$#
if [ $args -ne 3 ]
then
	echo "error: wrong number of arguments. Please provide 3 arguments"
	exit 0
fi

# check dir1 exists
# exit if not
if test ! -d $1
then
	echo "error: directory to move files from not found"
	exit 0
fi

# create dir2 if not exist
# check dir2 exists or was created
# exit if not
mkdir -p $2
if test ! -d $2
then
	echo "error: directory to move files from not found and/or failed to create one"
	exit 0
fi

# move all files from dir1 to dir2
# and rename them as 01, 02, ..., 09, 10, ...
declare -i n=0
for file in $1/*
do
	((n++))
	if test $n -lt 10
	then
		mv $file $2/0$n
	else
		mv $file $2/$n
	fi
done

# write to file № total files moved and renamed
echo "$n" " files were moved and renamed" > $2/$3

# check output file was successfully created
# print error if not
if test ! -f $2/$3
then
	echo "error: failed to create output file at " $2/$3
fi

echo "end of script"

